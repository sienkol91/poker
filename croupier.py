from game import *


class Croupier:
    def __init__(self):
        self.rules = {
            'start_cards': 2,
            'flop': 3,
            'turn': 1,
            'river': 1
        }

    def start_game(self):
        game = Game(['FF', 'FF', 'FF', 'TT', 'RR'])
        game.register_players(['Mysz', 'Olek', 'Tymon'], 500)
        game.draw_dealer()

        # start players with start cards
        game.hand_out(self.rules['start_cards'])

        game.print_players_with_their_cards()
        # game.print_common_cards()
        game.first_auction([5, 10])

        game.show_cards(self.rules['flop'])
        game.print_common_cards()
        game.auction()
        game.show_cards(self.rules['turn'])
        game.print_common_cards()
        game.show_cards(self.rules['river'])
        game.print_common_cards()

    def make_auction(self):
        pass
