class Player:
    def __init__(self, name):
        self.name = name
        self.cards = []
        self.notifications = []
        self.stack = 0
        self.dealer = False
        self.current_bet = 0
        self.active = True

    def set_stack(self, stack):
        self.stack = stack

    def add_to_stack(self, value):
        self.stack += value

    def reduce_stack(self, value):
        self.stack -= value

    def reduce_possible_and_return_reduced(self, value):
        if self.stack >= value:
            self.stack -= value
            return value
        else:
            reduced = self.stack
            self.stack = 0
            return reduced

    def get_card(self, card):
        self.cards.append(card)

    def notify_about_hand(self, hand):
        self.notifications.append(str("get hand " + hand))

    def print_cards(self):
        print self.name
        for card in self.cards:
            print card.card_name()
            pass

    def get_cards_as_string(self):
        cards_as_string = ""
        for index, card in enumerate(self.cards):
            cards_as_string += "[" + card.card_name() + "]"
        return cards_as_string

    def bet(self, stake):
        reduced = self.reduce_possible_and_return_reduced(stake)
        self.current_bet += reduced
        return reduced

    def get_decision(self, highest_bet):
        if self.stack == 0:
            return -1

        call = False
        if highest_bet > self.current_bet:
            call = True
            call_value = highest_bet - self.current_bet
            decision_str = "Call " + str(call_value)
        else:
            decision_str = "Check"
        decision = raw_input(self.name + " Fold [F]/" + decision_str + " [C]/Raise [number]: ")

        if decision.isdigit():
            decision = int(decision)
            return decision
        elif decision.isalnum() and (decision == 'C' or decision == 'c'):
            if call:
                return call_value
            else:
                return 0
        else:
            return -1
