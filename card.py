from math import *


class Card:
    def __init__(self, card):
        self.index = card
        self.figure = int(floor(card / 4))
        self.color = card % 4

    def card_figure(self):
        if floor(self.figure) < 9:
            return str(int(floor(self.figure)) + 2)
        elif floor(self.figure) == 9:
            return "J"
        elif floor(self.figure) == 10:
            return "Q"
        elif floor(self.figure) == 11:
            return "K"
        else:
            return "A"

    def card_color(self):
        if self.color == 0:
            return "s"
        elif self.color == 1:
            return 'h'
        elif self.color == 2:
            return "d"
        else:
            return "c"

    def card_name(self):
        return self.card_figure() + self.card_color()

#
# aspik = Card(48)
# print (aspik.figure)
