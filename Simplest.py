from random import *
from math import *

# 0 spades 1 hearts 2 diamonds 3 clubs
hand = sample(range(52), 5)


def card_figure(number):
    if floor(number / 4) < 9:
        return str(int(floor(number / 4)) + 2)
    elif floor(number / 4) == 9:
        return "J"
    elif floor(number / 4) == 10:
        return "Q"
    elif floor(number / 4) == 11:
        return "K"
    else:
        return "A"


def card_color(number):
    if number % 4 == 0:
        return "s"
    elif number % 4 == 1:
        return 'h'
    elif number % 4 == 2:
        return "d"
    else:
        return "c"


def card_name(number):
    return card_figure(number) + card_color(number)


for i in hand:
    print card_name(i),

figure = [int(floor(i / 4)) for i in hand]
color = [i % 4 for i in hand]
a = sorted(figure)
if len(set(color)) == 1 and (a == range(a[0], a[0] + 5) or a == [0, 1, 2, 3, 12]):
    print ("Straight flush")
elif len(set(figure)) == 2:
    if figure.count(figure[0]) == 1 or figure.count(figure[0]) == 4:
        print ("Four of a kind")
    else:
        print ("Full House")
elif len(set(color)) == 1:
    print ("Flush")
elif a == range(a[0], a[0] + 5) or a == [0, 1, 2, 3, 12]:
    print ("Straight")
elif len(set(figure)) == 3:
    if a.count(a[2]) == 3:
        print ("Three of a kind")
    else:
        print ("Two Pair")
elif len(set(figure)) == 4:
    print ("Pair")
else:
    print ("High card")
