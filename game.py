from random import shuffle, randint

from card import Card
from player import Player


class Game:
    def __init__(self, template):
        self.players = []
        self.deck = []
        self.fill_deck()
        self.common_cards = []
        self.template = template
        self.dealer_index = 0
        self.pot = 0
        self.current_player_index = 0
        self.player_accepts = 0
        self.highest_bet = 0

    def register_players(self, players, amount):
        for player in players:
            print 'registering ' + player + '...'
            self.register_player(player).set_stack(amount)

    def register_player(self, player_name):
        player = Player(player_name)
        self.players.append(player)
        return player

    def auction(self, set_current_player_index=True):
        if set_current_player_index:
            self.current_player_index = self.dealer_index
            self.next_player_index()

        self.player_accepts = 0
        while self.player_accepts < len(self.players):
            player = self.players[self.current_player_index]
            self.make_turn(player)
            self.next_player_index()
            # print decision
            # player_decision = let_player_decide()

    def make_turn(self, player):
        print '-----------------------------------------'
        print 'current stake: ' + str(self.pot)
        self.print_table()
        player_decision = player.get_decision(self.highest_bet)
        if player_decision < 0:
            self.player_accepts += 1
            print player.name + ' passes'
            pass
        elif player_decision == 0:
            self.player_accepts += 1
            print player.name + ' checks'
            pass
        else:
            self.player_bets(player, player_decision)
            print player.name + ' raises ' + str(player_decision)
            pass

    def player_bets(self, player, bet_value):
        # can be reduced
        bet_value = player.bet(bet_value)
        self.player_accepts = 1
        self.highest_bet = player.current_bet
        self.pot += bet_value

    def first_auction(self, blinds):
        if len(self.players) < 2:
            return
        self.current_player_index = self.dealer_index
        self.next_player_index()
        for blind in blinds:
            self.player_bets(self.players[self.current_player_index], blind)
            self.next_player_index()

        self.auction(False)

    def next_player_index(self):
        self.current_player_index = (self.current_player_index + 1) % len(self.players)

    def draw_dealer(self):
        if len(self.players) <= 1:
            self.dealer_index = 0
        else:
            self.dealer_index = randint(0, len(self.players) - 1)
        print self.dealer_index
        self.players[self.dealer_index].dealer = True

    def hand_out(self, cards_count):
        print 'handing out ' + str(cards_count) + ' card(s)...'
        for player in self.players:
            for card_no in range(cards_count):
                card = self.deck.pop()
                player.get_card(card)

    def show_cards(self, cards_count):
        print '-----------------------------------------'
        print 'showing ' + str(cards_count) + ' card(s)...'
        for i in range(cards_count):
            card = self.deck.pop()
            self.common_cards.append(card)

    def print_common_cards(self):
        common_cards_as_string = ""
        for i in range(len(self.template)):
            if len(self.common_cards) > i:
                common_cards_as_string += "[" + self.common_cards[i].card_name() + "]"
            else:
                common_cards_as_string += "[" + self.template[i] + "]"
        print common_cards_as_string

    def fill_deck(self):
        deck_indexes = range(52)
        shuffle(deck_indexes)
        for i in deck_indexes:
            card = Card(i)
            self.deck.append(card)

    def print_players_with_their_cards(self):
        for player in self.players:
            name = player.name
            if player.dealer:
                name += "*"
            print name + " (" + str(player.stack) + ")"
            print player.get_cards_as_string()
            # player.print_cards()

    def print_table(self):
        i = 0
        for player in self.players:
            name = ""
            if self.current_player_index == i:
                name += "--->"
            name += player.name
            if player.dealer:
                name += "*"
            print name + " (" + str(player.stack) + ") [" + str(player.current_bet) + "]"
            print player.get_cards_as_string()
            i += 1
